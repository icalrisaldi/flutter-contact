import 'package:permission_handler/permission_handler.dart';



Future<bool> getPermission(PermissionGroup permission) async {
  var status = await PermissionHandler().checkPermissionStatus(permission);

  if (status != PermissionStatus.granted ) {
    final groupStatus =
    await PermissionHandler().requestPermissions([permission]);

    status = groupStatus[permission] ?? PermissionStatus.unknown;
  }

  bool granted = status == PermissionStatus.granted;


  return Future<bool>.value(granted);
}