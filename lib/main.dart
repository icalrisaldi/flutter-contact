import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

/*import 'package:flutter_contact/contact.dart';
import 'package:flutter_contact/contact_events.dart';
import 'package:flutter_contact/contacts.dart';
import 'package:flutter_contact/generated/i18n.dart';
import 'package:flutter_contact/group.dart';
import 'package:flutter_contact/paging_iterable.dart';*/

import 'package:contacts_service/contacts_service.dart';

import 'CRUD.dart';
import 'ClassPenangkap.dart';
import 'model/IsiContact.dart';
import 'utils/permission.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  CRUD dbHelper = CRUD();
  Future<List<ClassPenangkap>> future;
  List<ClassPenangkap> list;


  @override
  void initState() {
//    _listenForPermissionStatus();
    getContact();
  }

  void getContact() async {
    Iterable<Contact> contacts = await ContactsService.getContacts();

    if (contacts.length>0) {
      dbHelper.deleteAll();
      for(Contact i in contacts){
            Iterable<Item> phone = await i.phones;
            for(Item j in phone){
              ClassPenangkap todo = new ClassPenangkap(i.displayName, j.value);
              dbHelper.insert(todo);
              break;
            }
          }

      updateListView();
    }
  }

  void updateListView() {
    setState(() {
      future = dbHelper.getContactList();
    });
  }

 void _listenForPermissionStatus() async {
   final contactsGranted = await getPermission(PermissionGroup.contacts);
   if (!contactsGranted) {
     throw new PlatformException(
       code: "PERMISSION_DENIED",
       message: "Resources access denied",
       details: null,
     );
   } else {
     /*await Contacts.streamContacts().forEach((contact) {
       print("${contact.displayName}");
     });*/
     Iterable<Contact> contacts = await ContactsService.getContacts();
     for(Contact i in contacts){
       Iterable<Item> phone = await i.phones;
       for(Item j in phone){
         ClassPenangkap todo = new ClassPenangkap(i.displayName, j.value);
         dbHelper.insert(todo);
         break;
       }
     }
   }
 }

  /*Future<void> requestPermission(Permission permission) async {
    final status = await permission.request();

    setState(() {
      print(status);
      _permissionStatus = status;
      print(_permissionStatus);
    });
  }*/

  Card cardo(ClassPenangkap contact) {
    return Card(
      color: Colors.white,
      elevation: 2.0,
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Colors.red,
          child: Icon(Icons.people),
        ),
        title: Text(
          contact.name,
        ),
        subtitle: Text(contact.phone.toString()),
        trailing: GestureDetector(
          child: Icon(Icons.delete),
          onTap: () async {
            int result = await dbHelper.delete(contact);
            if (result > 0) {
              updateListView();
            }
          },
        ),
        /*onTap: () async {
          var contact2 = await navigateToEntryForm(context, contact);
          if (contact2 != null) {
            int result = await dbHelper.update(contact2);
            if (result > 0) {
              updateListView();
            }
          }
        },*/
      ),
    );
  }


  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: FutureBuilder<List<ClassPenangkap>>(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ListView.builder(itemBuilder: (context, index){
              return Column(
                  children: snapshot.data.map((todo) => cardo(todo)).toList());
            }, itemCount: snapshot.data.length);
          } else if (snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child:  CircularProgressIndicator(),
            );
          } else {
            return SizedBox();
          }
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
